document.addEventListener("DOMContentLoaded", () => {
  const canvas = document.getElementById('root')
  console.log(canvas)

  const {width, height} = canvas
  const side = Math.min(width, height)

  const ctx = canvas.getContext("2d")

  ctx.lineWidth = 5
  ctx.strokeStyle = '#FF0000';
  ctx.beginPath()
  ctx.arc(
    width / 2,
    height / 2,
    side * 0.45,
    0,
    Math.PI * 2,
    true
  )
  ctx.stroke()
})
